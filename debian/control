Source: ruby-pdf-reader
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Cédric Boutillier <boutil@debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb,
               rake,
               ruby,
               ruby-afm,
               ruby-ascii85,
               ruby-hashery,
               ruby-rc4,
               ruby-rspec,
               ruby-ttfunk
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-pdf-reader.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-pdf-reader
Homepage: https://github.com/yob/pdf-reader
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all
Rules-Requires-Root: no

Package: ruby-pdf-reader
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ${misc:Depends},
         ${ruby:Depends},
         ${shlibs:Depends},
         ruby-afm,
         ruby-ascii85,
         ruby-hashery,
         ruby-rc4,
         ruby-ttfunk
Suggests: ruby-prawn
Description: Ruby library for accessing the content of PDF files
 The PDF::Reader library implements a PDF parser conforming as much as possible
 to the PDF specification from Adobe. It provides programmatic access to the
 contents of a PDF file with a high degree of flexibility.
 .
 This is primarily a low-level library that should be used as the foundation
 for higher level functionality. There are a few exceptions to support very
 common use cases like extracting text from a page.
